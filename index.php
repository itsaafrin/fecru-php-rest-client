<?php
/*
 * @author     Aafrin <info@aafrin.com>
 */

//include the library class
include('httpful.phar');

//Make a GET request with BASIC Authentication
$response = \Httpful\Request::get('http://localhost:8060/rest-service-fecru/server-v1')
					->authenticateWith('admin', '1234') 
					->expectsXml()  
					->send();

//Display the response
echo $response;

?>